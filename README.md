
<!-- README.md is generated from README.Rmd. Please edit that file -->

# squirrelshoc

<!-- badges: start -->
<!-- badges: end -->

The goal of squirrelshoc is to …

## Installation

You can install the development version of squirrelshoc like so:

``` r
# FILL THIS IN! HOW CAN PEOPLE INSTALL YOUR DEV PACKAGE?
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(squirrelshoc)
## basic example code
```

Ajouter du texte plus tard pour presenter mon package a mes futurs
utilisateurs

``` r
get_message_fur_color(primary_fur_color = "Red")
#> We will focus on Red squirrels
get_message_fur_color(primary_fur_color = "Black")
#> We will focus on Black squirrels
```
