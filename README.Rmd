---
output: github_document
---

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.path = "man/figures/README-",
  out.width = "100%"
)
```

# squirrelshoc

<!-- badges: start -->
<!-- badges: end -->

The goal of squirrelshoc is to ...

## Installation

You can install the development version of squirrelshoc like so:

``` r
# FILL THIS IN! HOW CAN PEOPLE INSTALL YOUR DEV PACKAGE?
```

## Example

This is a basic example which shows you how to solve a common problem:

```{r example}
library(squirrelshoc)
## basic example code
```


Ajouter du texte plus tard pour presenter mon package a mes futurs utilisateurs

```{r}
get_message_fur_color(primary_fur_color = "Red")
get_message_fur_color(primary_fur_color = "Black")
```






